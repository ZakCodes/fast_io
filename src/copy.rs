use prelude::*;
use std::mem::{uninitialized, size_of};
use std::slice;

macro_rules! impl_t {
($($t:ident),*) => {
$(impl Readable for $t {
    fn read<R: Read>(r: &mut R) -> Result<Self> {
        let mut result: $t = unsafe { uninitialized() };
        let buffer = unsafe { slice::from_raw_parts_mut(
            &mut result as *mut $t as *mut u8,
            size_of::<$t>()
        )};
        r.read(buffer)?;
        Ok(result)
    }
}

impl Writeable for $t {
    fn write<W: Write>(&self, w: &mut W) -> Result<()> {
        let buffer = unsafe { slice::from_raw_parts(
            self as *const $t as *const u8,
            size_of::<$t>()
        )};
        w.write(buffer)?;
        Ok(())
    }
})*
}
}

impl_t!(u8, u16, u32, u64, usize, i8, i16, i32, i64, isize, f32, f64);

#[cfg(test)]
mod test {
    use tests_prelude::*;

    #[test]
    fn clone() {
        let path = "clone";

        let ushort = 1u8;
        let umedium = 1u16;
        let uint = 1u32;
        let u_size = 1usize;

        let short = -11i8;
        let medium = -2500i16;
        let int = 420i32;
        let i_size = 69000isize;

        let float = 3.95f32;
        let double = 123.456;

        // Write
        {
            let mut file = File::create(path).unwrap();
            ushort.write(&mut file).unwrap();
            umedium.write(&mut file).unwrap();
            uint.write(&mut file).unwrap();
            u_size.write(&mut file).unwrap();

            short.write(&mut file).unwrap();
            medium.write(&mut file).unwrap();
            int.write(&mut file).unwrap();
            i_size.write(&mut file).unwrap();

            float.write(&mut file).unwrap();
            double.write(&mut file).unwrap();
        }
        // Read
        {
            let mut file = File::open(path).unwrap();

            assert_eq!(ushort, Readable::read(&mut file).unwrap());
            assert_eq!(umedium, Readable::read(&mut file).unwrap());
            assert_eq!(uint, Readable::read(&mut file).unwrap());
            assert_eq!(u_size, Readable::read(&mut file).unwrap());

            assert_eq!(short, Readable::read(&mut file).unwrap());
            assert_eq!(medium, Readable::read(&mut file).unwrap());
            assert_eq!(int, Readable::read(&mut file).unwrap());
            assert_eq!(i_size, Readable::read(&mut file).unwrap());

            assert_eq!(float, Readable::read(&mut file).unwrap());
            assert_eq!(double, Readable::read(&mut file).unwrap());
        }

        remove_file(path).unwrap();
    }
}