use prelude::*;

impl Readable for String {
    fn read<R: Read>(r: &mut R) -> Result<Self> {
        let mut bytes = vec![0u8; usize::read(r)?];
        r.read(bytes.as_mut_slice())?;
        Ok(unsafe {String::from_utf8_unchecked(bytes)})
    }
}

impl<'a> Writeable for String {
    fn write<W: Write>(&self, w: &mut W) -> Result<()> {
        let bytes = self.as_bytes();
        bytes.len().write(w)?;
        w.write(bytes)?;
        Ok(())
    }
}

impl<'a> Writeable for &'a str {
    fn write<W: Write>(&self, w: &mut W) -> Result<()> {
        let bytes = self.as_bytes();
        bytes.len().write(w)?;
        w.write(bytes)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use tests_prelude::*;

    #[test]
    fn clone() {
        let path = "str";
        let string_ref = "Hello World";
        let string = String::from(string_ref);

        // Write
        {
            let mut file = File::create(path).unwrap();
            string_ref.write(&mut file).unwrap();
            string.write(&mut file).unwrap();
        }
        // Read
        {
            let mut file = File::open(path).unwrap();
            assert_eq!(string, String::read(&mut file).unwrap());
            assert_eq!(string, String::read(&mut file).unwrap());
        }

        remove_file(path).unwrap();
    }
    #[derive(PartialEq, Copy, Clone, Debug)]
    struct Point(f64, f64);
}