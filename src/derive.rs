#[cfg(test)]
mod test {
    use tests_prelude::*;

    #[test]
    fn derive() {
        let path = "derive";

        let point = Point(5.9, 9.5);
        let vector = Vector { x: 2.0, y: 3.0, z: 59.1};
        let enum_a = Enum::A;
        let enum_b = Enum::B(Vector { x: 3.7, y: 796.8, z: 8.7 });
        let enum_c = Enum::C(15, String::from("Hello World"));

        // Write
        {
            let mut file = File::create(path).unwrap();

            point.write(&mut file).unwrap();
            vector.write(&mut file).unwrap();
            enum_a.write(&mut file).unwrap();
            enum_b.write(&mut file).unwrap();
            enum_c.write(&mut file).unwrap();
        }
        // Read
        {
            let mut file = File::open(path).unwrap();

            assert_eq!(point, Readable::read(&mut file).unwrap());
            assert_eq!(vector, Readable::read(&mut file).unwrap());
            assert_eq!(enum_a, Readable::read(&mut file).unwrap());
            assert_eq!(enum_b, Readable::read(&mut file).unwrap());
            assert_eq!(enum_c, Readable::read(&mut file).unwrap());
        }

        remove_file(path).unwrap();
    }
}