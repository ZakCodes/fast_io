use prelude::*;

impl<T: Readable+Sized> Readable for Box<[T]> {
    fn read<R: Read>(r: &mut R) -> Result<Self> {
        let vec: Vec<T> = Readable::read(r)?;
        Ok(vec.into_boxed_slice())
    }
}

impl<'a, T: Writeable+Sized> Writeable for &'a [T] {
    fn write<W: Write>(&self, w: &mut W) -> Result<()> {
        self.len().write(w)?;
        for t in self.iter() {
            t.write(w)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use tests_prelude::*;

    #[test]
    fn slice() {
        let path = "slice";
        let copy_a = [1.0, 5.9, 3.7];
        let copy_b = [0usize, 1usize, 5usize, 15usize];
        let derive_a = [Enum::A, Enum::B(Vector {x: 14.2 , y: -85.5 , z: 78.01}), Enum::C(15, String::from("Hello"))];
        let derive_b = [Point(15.9, 7.15), Point(-50.91, -72.12)];
        let vec_vec_str = [vec!["Hello"], vec!["World"]];
        let vec_vec_string = [vec![String::from("Hello")], vec![String::from("World")]];

        // Write
        {
            let mut file = File::create(path).unwrap();

            copy_a.as_ref().write(&mut file).unwrap();
            copy_b.as_ref().write(&mut file).unwrap();
            derive_a.as_ref().write(&mut file).unwrap();
            derive_b.as_ref().write(&mut file).unwrap();
            vec_vec_str.as_ref().write(&mut file).unwrap();
            vec_vec_string.as_ref().write(&mut file).unwrap();
        }
        // Read
        {
            let mut file = File::open(path).unwrap();

            //let array: Box<[f64]> = Readable::read(&mut file).unwrap();
            assert_eq!(copy_a, *Box::<[f64]>::read(&mut file).unwrap());
            assert_eq!(copy_b, *Box::<[usize]>::read(&mut file).unwrap());
            assert_eq!(derive_a, *Box::<[Enum]>::read(&mut file).unwrap());
            assert_eq!(derive_b, *Box::<[Point]>::read(&mut file).unwrap());
            assert_eq!(vec_vec_string, *Box::<[Vec<String>]>::read(&mut file).unwrap());
            assert_eq!(vec_vec_string, *Box::<[Vec<String>]>::read(&mut file).unwrap());
        }

        remove_file(path).unwrap();
    }
}