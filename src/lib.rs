#[macro_use]
extern crate clone_derive;

mod copy;
mod derive;
mod vec;
mod slice;
mod str;

pub mod prelude {
    pub use ::{Readable, Writeable};
    pub use std::io::{Read, Write, Result, Error, ErrorKind};
}

use std::io::{Read, Write, Result};

/// Trait to read objects from objects implementing the `Read` trait
pub trait Readable: Sized {
    /// Read an object from a `Read` stream
    fn read<R: Read>(r: &mut R) -> Result<Self>;
}

/// Trait to write objects to objects implementing the `Write` trait
pub trait Writeable: Sized {
    /// Write an object to a `Write` stream
    fn write<W: Write>(&self, w: &mut W) -> Result<()>;
}

#[cfg(test)]
mod tests_prelude {
    pub use prelude::*;
    pub use std::fs::{File, remove_file};

    #[derive(Debug, PartialEq, Readable, Writeable)]
    pub struct Point(pub f64, pub f64);

    #[derive(Debug, PartialEq, Readable, Writeable)]
    pub struct Vector {
        pub x: f64,
        pub y: f64,
        pub z: f64
    }

    #[derive(Debug, PartialEq, Readable, Writeable)]
    pub enum Enum {
        A,
        B(Vector),
        C(u32, String)
    }
}