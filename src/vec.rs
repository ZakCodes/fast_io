use prelude::*;

impl<T: Readable> Readable for Vec<T> {
    fn read<R: Read>(r: &mut R) -> Result<Self> {
        let size: usize = Readable::read(r)?;
        let mut result = Vec::with_capacity(size);
        for _ in 0..size {
            result.push(T::read(r)?);
        }
        Ok(result)
    }
}

impl<T: Writeable> Writeable for Vec<T> {
    fn write<W: Write>(&self, w: &mut W) -> Result<()> {
        self.len().write(w)?;
        for t in self {
            t.write(w)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use tests_prelude::*;

    #[test]
    fn vec() {
        let path = "vec";
        let copy_a = vec![1.0, 5.9, 3.7];
        let copy_b: Vec<usize> = (0usize..15usize).collect();
        let derive_a = vec![Enum::A, Enum::B(Vector {x: 14.2 , y: -85.5 , z: 78.01}), Enum::C(15, String::from("Hello"))];
        let derive_b = vec![Point(15.9, 7.15), Point(-50.91, -72.12)];
        let vec_vec_str = vec![vec!["Hello"], vec!["World"]];
        let vec_vec_string = vec![vec![String::from("Hello")], vec![String::from("World")]];

        // Write
        {
            let mut file = File::create(path).unwrap();

            copy_a.write(&mut file).unwrap();
            copy_b.write(&mut file).unwrap();
            derive_a.write(&mut file).unwrap();
            derive_b.write(&mut file).unwrap();
            vec_vec_str.write(&mut file).unwrap();
            vec_vec_string.write(&mut file).unwrap();
        }
        // Read
        {
            let mut file = File::open(path).unwrap();

            assert_eq!(copy_a, Vec::read(&mut file).unwrap());
            assert_eq!(copy_b, Vec::read(&mut file).unwrap());
            assert_eq!(derive_a, Vec::read(&mut file).unwrap());
            assert_eq!(derive_b, Vec::read(&mut file).unwrap());
            assert_eq!(vec_vec_string, Vec::<Vec<String>>::read(&mut file).unwrap());
            assert_eq!(vec_vec_string, Vec::<Vec<String>>::read(&mut file).unwrap());
        }

        remove_file(path).unwrap();
    }
}