extern crate proc_macro;
#[macro_use]
extern crate quote;
extern crate syn;

use proc_macro::TokenStream;
use syn::{Body, Ident, VariantData};

#[proc_macro_derive(Readable)]
pub fn readable(input: TokenStream) -> TokenStream {
    // Construct a string representation of the type definition
    let s = input.to_string();

    // Parse the string representation
    let ast = syn::parse_derive_input(&s).unwrap();

    // Get the name and the attributes of the type
    let name = &ast.ident;
    let body = match ast.body {
        Body::Struct(data) => match data {
            VariantData::Unit => {
                quote! {
                    Ok(#name())
                }
            }
            VariantData::Tuple(fields) => {
                let fields = fields.iter().map(|_| Ident::from("")).collect::<Vec<_>>();
                quote! {
                    Ok( #name(
                    #( #fields Readable::read(r)? ),*
                    ) )
                }
            }
            VariantData::Struct(fields) => {
                let field_names = fields.iter().map(|field| field.clone().ident.unwrap());
                quote! {
                    Ok(#name {
                        #( #field_names: Readable::read(r)? ),*
                    })
                }
            }
        },
        Body::Enum(variants) => {
            let arms = variants.iter().enumerate().map(|(index, variant)| {
                let ident = &variant.ident;
                let body = match &variant.data {
                    &VariantData::Unit => {
                        quote!{}
                    }
                    &VariantData::Tuple(ref fields) => {
                        let fields = fields.iter().map(|_| Ident::from(""));
                        quote! {
                            (#( #fields Readable::read(r)? ),*)
                        }
                    }
                    &VariantData::Struct(_) => {
                        quote! {
                            (Readable::read(r)?)
                        }
                    }
                };
                quote! {
                #index => #name::#ident #body
                }
            });
            quote! {
            let id: usize = Readable::read(r)?;
            Ok(match id {
                #(
                #arms,
                )*
                _ => return Err(Error::new(ErrorKind::InvalidData, "Index doesn't match any of the variants of the enum #name"))
            })
            }
        }
    };

    // Generate the implement block from the function body
    let gen = quote! {
    impl Readable for #name {
        fn read<R: Read>(r: &mut R) -> Result<Self> {
            #body
        }
    }
    };

    // Return the generated impl
    gen.parse().unwrap()
}

#[proc_macro_derive(Writeable)]
pub fn writeable(input: TokenStream) -> TokenStream {
    // Construct a string representation of the type definition
    let s = input.to_string();

    // Parse the string representation
    let ast = syn::parse_derive_input(&s).unwrap();

    // Get the name of the type
    let name = &ast.ident;
    let body = match ast.body {
        Body::Struct(data) => match data {
            VariantData::Unit => {
                quote!{}
            }
            VariantData::Tuple(fields) => {
                let fields = (0..fields.len()).map(|index| Ident::from(format!("{:?}", index)));

                quote! {
                #( self.#fields.write(w)?; )*
                }
            }
            VariantData::Struct(fields) => {
                let fields = fields.iter().map(|field| field.clone().ident.unwrap());
                quote! {
                #( self.#fields.write(w)?; )*
                }
            }
        },
        Body::Enum(variants) => {
            let arms = variants.iter().enumerate().map(|(index, variant)| {
                let ident = &variant.ident;
                match &variant.data {
                    &VariantData::Unit => {
                        quote! {
                        &#name::#ident => {
                            #index.write(w)?;
                        }
                        }
                    }
                    &VariantData::Tuple(ref fields) => {
                        let fields = fields
                            .iter()
                            .enumerate()
                            .map(|(index, _)| Ident::from(format!("arg_{}", index)))
                            .collect::<Vec<_>>();
                        let fields_2 = fields.clone();

                        quote! {
                        &#name::#ident( #(ref #fields),* ) => {
                            #index.write(w)?;
                            #( #fields_2.write(w)?; )*
                        }
                        }
                    }
                    &VariantData::Struct(_) => {
                        quote! {
                        &#name::#ident(content) => {
                            #index.write(w)?;
                            content.write(w)?;
                        }
                        }
                    }
                }
            });

            quote! {
            match self {
                #(#arms),*
            }
            }
        }
    };

    // Generate the impl block with the body of the function
    let gen = quote! {
    impl Writeable for #name {
        fn write<W: Write>(&self, w: &mut W) -> Result<()> {
            #body
            Ok(())
        }
    }
    };

    // Return the generated impl
    gen.parse().unwrap()
}